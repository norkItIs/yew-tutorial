# Yew Tutorial

Version with external API calls, slightly modified and simplified. Reference [https://yew.rs/docs/tutorial](https://yew.rs/docs/tutorial)

Called API is

    http://httpbin.org/ip

Run with

    trunk serve --port 8081
