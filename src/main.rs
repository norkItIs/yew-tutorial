use gloo_net::http::Request;
use serde::Deserialize;
use wasm_bindgen::prelude::*;
use yew::prelude::*;

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

macro_rules! console_log {
    // Note that this is using the `log` function imported above during
    // `bare_bones`
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}

#[derive(Clone, PartialEq, Deserialize)]
struct Ip {
    origin: String,
}

#[derive(Properties, PartialEq)]
struct IpDetailsProps {
    ip: Ip,
}

#[derive(Properties, PartialEq)]
struct IpListProps {
    ips: Vec<Ip>,
    on_click: Callback<Ip>,
}

#[function_component(IpList)]
fn ip_list(IpListProps { ips, on_click }: &IpListProps) -> Html {
    let on_click = on_click.clone();
    ips.iter()
        .map(|ip| {
            let on_ip_select = {
                let on_click = on_click.clone();
                let ip = ip.clone();
                Callback::from(move |_| on_click.emit(ip.clone()))
            };
            html! {
                <p onclick={on_ip_select}>{format!("Ip: {}", ip.origin)}</p>
            }
        })
        .collect()
}

#[function_component(IpDetails)]
fn ip_details(IpDetailsProps { ip }: &IpDetailsProps) -> Html {
    html! {
        <>
            <h3>{ ip.origin.clone() }</h3>
            <img src="https://via.placeholder.com/640x360.png?text=VideoPlayer+Placeholder" alt="video thumbnail" />
        </>
    }
}

#[function_component(App)]
fn app() -> Html {
    console_log!("App started");
    let ips = use_state(|| vec![]);
    {
        let ips = ips.clone();
        use_effect_with_deps(move |_| {
            let ips = ips.clone();
            wasm_bindgen_futures::spawn_local(async move {
                let fetched_ip: Ip = Request::get("http://httpbin.org/ip")
                    .send()
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap();
                ips.set(vec![fetched_ip]);
            });
            || ()
        }, ());
    }

    let selected_ip = use_state(|| None);
    let on_ip_select = {
        let selected_ip = selected_ip.clone();
        Callback::from(move |ip: Ip| selected_ip.set(Some(ip)))
    };

    let details = selected_ip.as_ref().map(|ip| {
        html! {
            <IpDetails ip={ip.clone()} />
        }
    });
    html! {
        <>
            <IpList ips={ (*ips).clone() } on_click={on_ip_select.clone()}/>
            { for details }
        </>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
